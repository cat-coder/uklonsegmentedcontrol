//
//  UklonSegmentedView.swift
//  CarTypeViewSample
//
//  Created by ALEX NEMINSKY on 5/6/18.
//  Copyright © 2018 ALEX NEMINSKY. All rights reserved.
//

import UIKit

protocol UklonSegmentedViewProtocol: class {
    
    /// Background color for item
    var bgItemColor: UIColor { get set }
    
    /// Color for title font
    var fontColor: UIColor { get set }
    
    /// Color for title font in selected item
    var fontSelectedColor: UIColor { get set }
    
    /// Background color for slider
    var bgSliderColor: UIColor { get set }
    
    /// Color for slider selector
    var selectedSliderColor: UIColor { get set }
    
    var delegate: UklonSegmentedViewDelegate? { get set }
    
    /// Setup items array for control
    ///
    /// - Parameter array: optional array with UklonItemModel. If nil - control is empty.
    func setItems(from array: [UklonItemModel]?)
    
}

class UklonSegmentedView: UIView {
    
    //MARK: - Properties
    
    //Colors
    var bgItemColor: UIColor = .white {
        didSet { collectionView.backgroundColor = bgItemColor }
    }
    var fontColor: UIColor = .black {
        didSet {
            collectionView.reloadData()
        }
    }
    var fontSelectedColor: UIColor = .red {
        didSet {
            collectionView.reloadData()
        }
    }
    var bgSliderColor: UIColor = .yellow {
        didSet {
            sladeView.bgColor = bgSliderColor
        }
    }
    var selectedSliderColor: UIColor = .red {
        didSet {
            sladeView.selectColor = selectedSliderColor
        }
    }
    
    weak var delegate: UklonSegmentedViewDelegate?
    
    private var items: [UklonItemModel]?
    private var selectedItem: Int = 0 {
        didSet { setSlider(animated: true) }
    }
    
    //Views
    private let collectionLayout: UICollectionViewFlowLayout
    private let collectionView: UICollectionView
    private let sladeView: SliderView
    
    override var bounds: CGRect {
        didSet {
            collectionView.reloadData()
            setSlider(animated: false)
        }
    }
    
    private var allConstraints: [NSLayoutConstraint] = []
    
    let cellHeight: CGFloat = 68.0
    let minCellWidth: CGFloat = 80.0
    
    //MARK: - Inits
    override init(frame: CGRect) {
        collectionLayout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionLayout)
        sladeView = SliderView(frame: CGRect.zero)
        super.init(frame: frame)
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        collectionLayout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionLayout)
        sladeView = SliderView(frame: CGRect.zero)
        super.init(coder: aDecoder)
        commonSetup()
    }

    //MARK: - Public methods
    func setItems(from array: [UklonItemModel]?) {
        items = array
    }
    
    //MARK: - Private methods
    //MARK: Setup views
    
    private func commonSetup() {
        self.backgroundColor = .clear
        setupCollectionView()
        setupSladeView()
        setupConstraints()
        setSlider(animated: false)
    }
    
    private func setupCollectionView() {
        collectionView.backgroundColor = bgItemColor
        collectionLayout.minimumLineSpacing = 0
        collectionLayout.minimumInteritemSpacing = 0
        collectionLayout.sectionInset = UIEdgeInsets.zero
        collectionLayout.scrollDirection = .horizontal
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(UklonSegmentedViewCell.self, forCellWithReuseIdentifier: UklonSegmentedViewCell.cellIdString)
        collectionView.delegate = self
        collectionView.dataSource = self
        self.addSubview(collectionView)   
    }
    
    private func setupSladeView() {
        sladeView.bgColor = bgSliderColor
        sladeView.selectColor = selectedSliderColor
        sladeView.animationDuration = 0.25
        self.addSubview(sladeView)
    }

    func setupConstraints() {
        let views: [String: Any] = [
            "collectionView": collectionView,
            "sladeView": sladeView
        ]
        
        //collection View
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        let collectionViewHeight: Int = 68
        let collectionViewV: String = "V:|-0-[collectionView(\(collectionViewHeight)@250)]-0-[sladeView]"
        let collectionViewH: String = "H:|-0-[collectionView]-0-|"
        let collectionViewVConstraints = NSLayoutConstraint.constraints(withVisualFormat: collectionViewV, metrics: nil, views: views)
        let collectionViewHConstraints = NSLayoutConstraint.constraints(withVisualFormat: collectionViewH, metrics: nil, views: views)
        allConstraints += collectionViewVConstraints
        allConstraints += collectionViewHConstraints

        //slade View
        sladeView.translatesAutoresizingMaskIntoConstraints = false
        let sladeViewHeight: Int = 2
        let sladeViewV: String = "V:[collectionView]-0-[sladeView(\(sladeViewHeight))]-0-|"
        let sladeViewH: String = "H:|-0-[sladeView]-0-|"
        let sladeViewVConstraints = NSLayoutConstraint.constraints(withVisualFormat: sladeViewV, metrics: nil, views: views)
        let sladeViewHConstraints = NSLayoutConstraint.constraints(withVisualFormat: sladeViewH, metrics: nil, views: views)
        allConstraints  += sladeViewVConstraints
        allConstraints  += sladeViewHConstraints
        
        NSLayoutConstraint.activate(allConstraints)
    }
    
    private func setSlider(animated: Bool) {
        guard let cellWidth = getCellSize()?.width else { return }
        let collectionViewOffset = collectionView.contentOffset.x
        let inset = CGFloat(selectedItem) * cellWidth - collectionViewOffset
        sladeView.slideSelector(inset: inset, width: cellWidth, anitated: animated)
    }
}

//MARK: - UICollectionViewDelegate
extension UklonSegmentedView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        selectedItem = index
        delegate?.didTapItem(with: index, control: self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        setSlider(animated: false)
    }
    
}

//MARK: - UICollectionViewDataSource
extension UklonSegmentedView: UICollectionViewDataSource {

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let items = items else {
            fatalError()
        }
        let item = items[indexPath.row]
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: UklonSegmentedViewCell.cellIdString, for: indexPath) as! UklonSegmentedViewCell
        let isSelectedCell: Bool = indexPath.row == selectedItem
        cell = setupCell(cell, with: item, isSelected: isSelectedCell)
        if isSelectedCell {
            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: [])
        }
        return cell
    }
    
    //helpers
    func setupCell(_ cell: UklonSegmentedViewCell, with item: UklonItemModel,  isSelected: Bool) -> UklonSegmentedViewCell {
        cell.fontColor = fontColor
        cell.selectedFontColor = fontSelectedColor
        cell.backgroundColor = bgItemColor
        cell.configure(with: item)
        cell.isSelected = isSelected
        return cell
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension UklonSegmentedView: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let size = getCellSize() else { fatalError() }
        return size
    }
    
    //helpers
    private func getCellSize() -> CGSize? {
        
        guard let cellsCount = items?.count else {
            return nil
        }

        let viewWidth: CGFloat = frame.width
        var cellWidth: CGFloat = viewWidth / CGFloat(cellsCount)
        
        var isCollectionScrollEnable: Bool
        
        if cellWidth >= minCellWidth {
            //scrolling is disabled
            isCollectionScrollEnable = false
        } else {
            isCollectionScrollEnable = true
            cellWidth = minCellWidth
        }
        collectionView.isScrollEnabled = isCollectionScrollEnable
        return CGSize(width: cellWidth, height: cellHeight)
    }
}
