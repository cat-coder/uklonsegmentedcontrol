//
//  UklonSegmentedViewDelegate.swift
//  CarTypeViewSample
//
//  Created by ALEX NEMINSKY on 5/6/18.
//  Copyright © 2018 ALEX NEMINSKY. All rights reserved.
//

import Foundation

@objc protocol UklonSegmentedViewDelegate: class {

    func didTapItem(with index: Int, control: UklonSegmentedView)
    
}
