//
//  SliderView.swift
//  CarTypeViewSample
//
//  Created by ALEX NEMINSKY on 5/7/18.
//  Copyright © 2018 ALEX NEMINSKY. All rights reserved.
//

import UIKit

class SliderView: UIView {
    
    //MARK: - Properties
    //Views
    let selectorView: UIView = UIView(frame: CGRect.zero)
    
    //Colors
    var bgColor: UIColor = .white {
        didSet { backgroundColor = bgColor }
    }
    var selectColor: UIColor = .black {
        didSet { selectorView.backgroundColor = selectColor }
    }
    
    var animationDuration: TimeInterval = 0.5
    
    override var bounds: CGRect {
        didSet {
            if bounds != CGRect.zero && selectorView.bounds.height == 0.0 {
                selectorView.frame = CGRect(origin: selectorView.bounds.origin, size: CGSize(width: selectorView.bounds.width, height: bounds.height))
            }
        }
    }
    
    //MARK: - Inits
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
    //MARK: - Public methods
    func slideSelector(inset: CGFloat, width: CGFloat, anitated: Bool = true) {
        let newFrame = getSelectorFrame(inset: inset, width: width)
        if anitated {
            UIView.animate(withDuration: animationDuration) {
                self.setSelectorFrame(to: newFrame)
            }
        } else {
            setSelectorFrame(to: newFrame)
        }
    }
    
    //MARK: - Private methods
    //MARK: Setup views
    
    private func commonSetup() {
        backgroundColor = bgColor
        selectorView.backgroundColor = selectColor
        addSubview(selectorView)
    }
    
    private func getSelectorFrame(inset: CGFloat, width: CGFloat) -> CGRect {
        let height = bounds.height
        let newFrame = CGRect(x: inset, y: 0.0, width: width, height: height)
        return newFrame
    }
    
    private func setSelectorFrame(to newFrame: CGRect) {
        selectorView.frame = newFrame
    }
}
