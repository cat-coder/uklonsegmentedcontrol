//
//  UklonSegmentedViewCell.swift
//  CarTypeViewSample
//
//  Created by ALEX NEMINSKY on 5/6/18.
//  Copyright © 2018 ALEX NEMINSKY. All rights reserved.
//

import UIKit

class UklonSegmentedViewCell: UICollectionViewCell {
    
    //MARK: - Properties
    
    static let cellIdString = "uklonSegmentedViewCell"
    
    //colors
    var fontColor: UIColor = .gray
    var selectedFontColor: UIColor = .black
    
    private var item: UklonItemModel? {
        didSet {
            setupViewsWith(item: item)
        }
    }
    private var allConstraints: [NSLayoutConstraint] = []
    
    //MARK: Views
    let imageView: UIImageView = UIImageView(frame: CGRect.zero)
    let titleLabel: UILabel = UILabel(frame: CGRect.zero)
    
    override var isSelected: Bool {
        didSet {
            setState(selected: isSelected)
        }
    }
    
    //MARK: - Inits
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
    //MARK: - Public methods
    func configure(with item: UklonItemModel) {
        self.item = item
    }
    
    //MARK: - Private methods
    private func commonSetup() {
        setupImage()
        setupTitle()
        setupConstraints()
    }
    
    private func setupImage() {
        imageView.contentMode = .center
        imageView.backgroundColor = .clear
        addSubview(imageView)
    }
    
    private func setupTitle() {
        titleLabel.font = UIFont.systemFont(ofSize: 8)
        titleLabel.textAlignment = .center
        titleLabel.backgroundColor = .clear
        addSubview(titleLabel)
    }
    
    private func setupConstraints() {
        let views: [String: Any] = [
            "imageView": imageView,
            "titleLabel": titleLabel
        ]
        
        //setup imageView
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let imageHeight: Int = 36
        let imageViewV: String = "V:|-8-[imageView(\(imageHeight)@250)]-4-[titleLabel]"
        let imageViewH: String = "H:|-8-[imageView]-8-|"
        let imageViewVConstraints = NSLayoutConstraint.constraints(withVisualFormat: imageViewV, metrics: nil, views: views)
        let imageViewHConstraints = NSLayoutConstraint.constraints(withVisualFormat: imageViewH, metrics: nil, views: views)
        allConstraints += imageViewVConstraints
        allConstraints += imageViewHConstraints
        
        //setup titleLabel
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        let titleLabelHeight: Int = 12
        let titleLabelV: String = "V:[titleLabel(\(titleLabelHeight))]-8-|"
        let titleLabelH: String = "H:|-8-[titleLabel]-8-|"
        let titleLabelVConstraints = NSLayoutConstraint.constraints(withVisualFormat: titleLabelV, metrics: nil, views: views)
        let titleLabelHConstraints = NSLayoutConstraint.constraints(withVisualFormat: titleLabelH, metrics: nil, views: views)
        allConstraints  += titleLabelVConstraints
        allConstraints  += titleLabelHConstraints
        
        NSLayoutConstraint.activate(allConstraints)
    }

    private func setState(selected: Bool) {
        imageView.image = isSelected ? item?.iconSelectedImage : item?.iconImage
        titleLabel.textColor = isSelected ? selectedFontColor : fontColor
    }
    
    private func setupViewsWith(item: UklonItemModel?) {
        guard let item = item else {
            titleLabel.text = nil
            imageView.image = nil
            return
        }
        titleLabel.text = item.titleText
        imageView.image = isSelected ? item.iconSelectedImage : item.iconImage
    }
}
