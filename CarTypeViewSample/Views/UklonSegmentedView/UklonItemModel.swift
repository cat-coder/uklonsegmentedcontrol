//
//  UklonItemModel.swift
//  CarTypeViewSample
//
//  Created by ALEX NEMINSKY on 5/6/18.
//  Copyright © 2018 ALEX NEMINSKY. All rights reserved.
//

import UIKit

struct UklonItemModel {
    let titleText: String
    let iconImage: UIImage
    let iconSelectedImage: UIImage
}

extension UklonItemModel {
    init(with carType: CarTypeModel) {
        titleText = carType.title
        iconImage = UIImage(named: carType.imageName)!
        iconSelectedImage = UIImage(named: carType.selectedImageName)!
    }
}
