//
//  AppConstants.swift
//  CarTypeViewSample
//
//  Created by ALEX NEMINSKY on 5/6/18.
//  Copyright © 2018 ALEX NEMINSKY. All rights reserved.
//

import UIKit

struct AppConstants {
    
    //MARK: - Colors
    struct Colors {
        static let blue = UIColor(r: 73, g: 192, b: 235)
        static let yellow = UIColor(r: 254, g: 208, b: 0)
        static let greyBackground = UIColor(r: 69, g: 71, b: 84)
        static let greyText = UIColor(r: 156, g: 156, b: 156)
    }
 
}
