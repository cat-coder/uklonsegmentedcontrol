//
//  UIColor+Extension.swift
//  CarTypeViewSample
//
//  Created by ALEX NEMINSKY on 5/7/18.
//  Copyright © 2018 ALEX NEMINSKY. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(r: Int, g: Int, b: Int, a: CGFloat = 1.0) {
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: a)
    }
    
}
