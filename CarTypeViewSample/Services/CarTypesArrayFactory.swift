//
//  CarTypesArrayFactory.swift
//  CarTypeViewSample
//
//  Created by ALEX NEMINSKY on 5/6/18.
//  Copyright © 2018 ALEX NEMINSKY. All rights reserved.
//

import Foundation

protocol CarTypesArrayFactoryProtocol {
    
    /// Datasets for sample app
    ///
    /// - Returns: Array with 5 car types
    func getShortArray() -> [CarTypeModel]
    
    //// - Returns: Array with 5 car types
    func getLongArray() -> [CarTypeModel]
    
}

class CarTypesArrayFactory: CarTypesArrayFactoryProtocol {
    
    func getShortArray() -> [CarTypeModel] {
        let typesArray: [CarTypeModel] = [
            CarTypeModel(title: "Стандарт", imageName: "ic_car_standart", selectedImageName: "ic_car_standart_select"),
            CarTypeModel(title: "Комфорт", imageName: "ic_car_comfort", selectedImageName: "ic_car_comfort_select"),
            CarTypeModel(title: "Бизнес", imageName: "ic_car_business", selectedImageName: "ic_car_business_select"),
            CarTypeModel(title: "Универсал", imageName: "ic_car_universal", selectedImageName: "ic_car_universal_select"),
            CarTypeModel(title: "Микроавтобус", imageName: "ic_car_bus", selectedImageName: "ic_car_bus_select"),
        ]
        return typesArray
    }
    
    func getLongArray() -> [CarTypeModel] {
        let shortArray = self.getShortArray()
        let longArray = shortArray + shortArray
        return longArray
    }
    
}
