//
//  CarType.swift
//  CarTypeViewSample
//
//  Created by ALEX NEMINSKY on 5/6/18.
//  Copyright © 2018 ALEX NEMINSKY. All rights reserved.
//

import Foundation

struct CarTypeModel {
    let title: String
    let imageName: String
    let selectedImageName: String
}
