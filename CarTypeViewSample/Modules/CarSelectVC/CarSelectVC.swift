//
//  ViewController.swift
//  CarTypeViewSample
//
//  Created by ALEX NEMINSKY on 5/6/18.
//  Copyright © 2018 ALEX NEMINSKY. All rights reserved.
//

import UIKit

protocol CarSelectViewProtocol: class {
    
    /// Update data for controls
    func updatedData()
}

class CarSelectVC: UIViewController, CarSelectViewProtocol {

    //MARK: - Outlets
    @IBOutlet weak var firstControl: UklonSegmentedView!
    @IBOutlet weak var secondControl: UklonSegmentedView!

    //MARK: Properties
    var presenter: CarSelectPresenterProtocol!

    //MARK: - Life cycle
    override func viewDidLoad() {
        presenter = CarSelectPresenter(with: self)
        super.viewDidLoad()
        commonSetup()
        updatedData()
    }
    
    //MARK: - CarSelectViewProtocol methods
    func updatedData() {
        setItems()
    }
    
    //MARK: - Private methods
    private func commonSetup() {
        view.backgroundColor = .lightGray
        setup(control: firstControl)
        setup(control: secondControl)
    }
    
    private func setup(control: UklonSegmentedView) {
        
        control.bgItemColor = AppConstants.Colors.greyBackground
        control.fontColor = AppConstants.Colors.greyText
        control.fontSelectedColor = AppConstants.Colors.blue
        control.bgSliderColor = AppConstants.Colors.blue
        control.selectedSliderColor = AppConstants.Colors.yellow
        
        control.delegate = self
    }
    
    private func setItems() {
        firstControl.setItems(from: presenter.shortItems)
        secondControl.setItems(from: presenter.longItems)
    }

}

extension CarSelectVC: UklonSegmentedViewDelegate {
    func didTapItem(with index: Int, control: UklonSegmentedView) {
        
        var controlIndex: Int?
        
        if control.isEqual(firstControl) {
            controlIndex = 0
        } else if control.isEqual(secondControl) {
            controlIndex = 1
        }
        
        if let controlIndex = controlIndex {
            presenter.didTapButton(index: index, controlIndex: controlIndex)
        }
    }
}

