//
//  CarSelectPresenter.swift
//  CarTypeViewSample
//
//  Created by ALEX NEMINSKY on 5/6/18.
//  Copyright © 2018 ALEX NEMINSKY. All rights reserved.
//

import Foundation

protocol CarSelectPresenterProtocol {
    
    /// Array with 5 values
    var shortItems: [UklonItemModel]? { get }
    
    /// Array with 10 values
    var longItems: [UklonItemModel]? { get }
    
    /// Button tap event
    ///
    /// - Parameters:
    ///   - index: tapped button index
    ///   - controlIndex: controller number
    func didTapButton(index: Int, controlIndex: Int)
    
}

class CarSelectPresenter: CarSelectPresenterProtocol {
    
    //MARK: - Properties
    var shortItems: [UklonItemModel]?
    var longItems: [UklonItemModel]?
    
    private weak var view: CarSelectViewProtocol?

    private var shortCarTypesArray: [CarTypeModel]?
    private var longCarTypesArray: [CarTypeModel]?
    
    //MARK: - Init
    init(with view: CarSelectViewProtocol) {
        self.view = view
        setupDataSets()
    }
    
    //MARK: - Public methods
    func didTapButton(index: Int, controlIndex: Int) {
        print("didTapButton index: \(index) in control with index: \(controlIndex)")
        //handle controls button tap
    }

    //MARK: - Private methods
    private func setupDataSets() {
        let carTypesFactory: CarTypesArrayFactoryProtocol = CarTypesArrayFactory()
        
        let shortCarTypesArray = carTypesFactory.getShortArray()
        let longCarTypesArray = carTypesFactory.getLongArray()
        
        shortItems = shortCarTypesArray.map { UklonItemModel.init(with: $0) }
        longItems = longCarTypesArray.map { UklonItemModel.init(with: $0) }
    }   
}
